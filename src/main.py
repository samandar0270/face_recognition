import cv2
import tkinter as tk
from PIL import Image, ImageTk
from simple_facerec import SimpleFacerec

sfr = SimpleFacerec()
sfr.load_encoding_images("images/")

window = tk.Tk()
window.title("Face reconition app")
window.geometry("900x500")
window.resizable(False, False)
window.configure(bg="gray")

f1 = tk.LabelFrame(window, bg="black")
f1.pack(side="left")
l1 =tk.Label(f1, text="Name", font = ("times new roman", 20, "bold"), bg="black", fg="white").pack(side="top", anchor="n")
label_name = tk.Label(f1, bg="black", width=20,fg="white", font=("Arial", 20, "bold"))
label_name.pack()
f2 = tk.LabelFrame(window, bg="black")
f2.pack(side="right",)
L1 = tk.Label(f2, text="Camera", font = ("times new roman", 20, "bold"), bg="black", fg="white").pack(side="top", anchor="n")
L2 = tk.Label(f2, bg="green")
L2.pack()

cap = cv2.VideoCapture(0)

while True:
    img = cap.read()[1]
    face_name = sfr.detect_known_faces(img)[1]
    img = cv2.flip(img, 1)
    img1 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
    img = ImageTk.PhotoImage(Image.fromarray(img1))
    L2["image"]=img
    for name in face_name:
        label_name["text"]=name
    window.update()

cap.release()
